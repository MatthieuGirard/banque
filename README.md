Projet Banque
-------------
Exercice Multi-threading

La classe Compte définit un compte avec un montant initial sur lequel il est possible d'effectuer des opérations de débit ou de crédit. Le découvert n'est pas autorisé.

La classe Banque définit une liste de comptes avec un montant initial identique. La méthode transfert permet le débit d'un montant d'un compte débiteur vers un compte créditeur pour autant que le débiteur soit suffisamment approvisionné. La méthode consistent permet de vérifier que la somme totale de l'argent en banque est inchangée après des opérations de transfert (transferts internes à la banque).

Ecrire une classe Transferts pour permettre l'exécution d'un nombre donné de transferts dans une banque. Les comptes débiteurs et créditeurs, ainsi que le montant (entre 1 et 5) de chaque transfert seront tirés aléatoirement. L'exécution de ces transferts se fera dans une méthode qui doit pouvoir s'exécuter en mode asynchrone (threading).

Créez une classe de test JUnit dans laquelle on activera 1000 fois simultanément la méthode de transfert sur une banque de 10 comptes. Chaque exécution de la méthode de transfert générera 1000 transferts. On aura donc effectué 1000*1000 transferts en tout.

Vérifiez automatiquement qu'il n'y a pas, à l'issue de ces transferts, d'argent perdu ou gagné dans la banque (méthode consistent). En cas de problème, corrigez-le !